<?php

namespace ThibaudDauce;

use Carbon\Carbon;

class PeriodPresenter
{
    protected $start;
    protected $end;

    public function __construct(Carbon $start, Carbon $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function present()
    {
        if ($this->start->day === 1 and $this->end->day === 1) {
            $presentFrom = $this->start->formatLocalized('%B %Y');
            $presentTo = $this->lastMonth()->formatLocalized('%B %Y');
        } else {
            $presentFrom = trim($this->start->formatLocalized('%e %B %Y'));
            $presentTo = trim($this->end->formatLocalized('%e %B %Y'));
        }

        if ($this->start->day === 1 and $this->quantity() === 1) {
            return $presentFrom;
        }

        if ($this->start->day === 1 and $this->end->day === 1 and $this->quantity() === 2) {
            return "$presentFrom et $presentTo";
        }

        if ($this->start->day === 1 and $this->end->day === 1) {
            return "de $presentFrom à $presentTo";
        } else {
            return "du $presentFrom au $presentTo";
        }
    }

    protected function lastMonth(): Carbon
    {
        return $this->end->copy()->subMonth();
    }

    protected function quantity(): int
    {
        return (($this->end->month + 12) - $this->start->month) % 12;
    }
}

<?php

namespace ThibaudDauce;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    function setUp()
    {
        setlocale(LC_TIME, 'fr_FR.utf8');
    }

    /** @test */
    function can present a one month period()
    {
        $period = new PeriodPresenter(
            Carbon::parse('2017-02-01'),
            Carbon::parse('2017-03-01')
        );

        $this->assertEquals('février 2017', $period->present());
    }

    /** @test */
    function can present a two months period()
    {
        $period = new PeriodPresenter(
            Carbon::parse('2017-02-01'),
            Carbon::parse('2017-04-01')
        );

        $this->assertEquals('février 2017 et mars 2017', $period->present());
    }

    /** @test */
    function can present a three months period()
    {
        $period = new PeriodPresenter(
            Carbon::parse('2017-02-01'),
            Carbon::parse('2017-05-01')
        );

        $this->assertEquals('de février 2017 à avril 2017', $period->present());
    }

    /** @test */
    function can present a period smaller than one month()
    {
        $period = new PeriodPresenter(
            Carbon::parse('2017-02-15'),
            Carbon::parse('2017-03-01')
        );

        $this->assertEquals('du 15 février 2017 au 1 mars 2017', $period->present());
    }

    /** @test */
    function can present a yearly period()
    {
        $period = new PeriodPresenter(
            Carbon::parse('2017-02-01'),
            Carbon::parse('2018-02-01')
        );

        $this->assertEquals('de février 2017 à janvier 2018', $period->present());
    }

    /** @test */
    function can present a one year period starting in the middle of a month()
    {
        $period = new PeriodPresenter(
            Carbon::parse('2017-02-15'),
            Carbon::parse('2018-02-15')
        );

        $this->assertEquals('du 15 février 2017 au 15 février 2018', $period->present());
    }
}

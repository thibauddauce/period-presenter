# Period Presenter

Present a period in a human readable format. Works only in french right now. I'm open to Pull Requests.

## Installation

```bash
composer require thibaud-dauce/period-presenter
```

```php
setlocale(LC_TIME, 'fr_FR.utf8');
```

## Usage

### Single month

```php
$period = new PeriodPresenter(
    Carbon::parse('2017-02-01'),
    Carbon::parse('2017-03-01')
);

$this->assertEquals('février 2017', $period->present());
```

### Two months

```php
$period = new PeriodPresenter(
    Carbon::parse('2017-02-01'),
    Carbon::parse('2017-04-01')
);

$this->assertEquals('février 2017 et mars 2017', $period->present());
```

### More than two months

```php
$period = new PeriodPresenter(
    Carbon::parse('2017-02-01'),
    Carbon::parse('2017-05-01')
);

$this->assertEquals('de février 2017 à avril 2017', $period->present());
```

## Less than one month

```php
$period = new PeriodPresenter(
    Carbon::parse('2017-02-15'),
    Carbon::parse('2017-03-01')
);

$this->assertEquals('du 15 février 2017 au 1 mars 2017', $period->present());
```
